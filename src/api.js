import axios from 'axios'

const HOST = 'http://localhost:3000'

export const getItems = () => axios.get(`${HOST}/search`)
  .then(({ data }) => data)


export const getItem = id =>
  axios.get(`${HOST}/item/:${id}`).then(({ data }) => data);
