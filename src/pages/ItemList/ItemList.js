import React from "react";
import { connect } from "react-redux";
import { REQUEST_STATUS } from "../../constants";
import { getItems } from "../../api";
import {
  itemListFailure,
  itemListRequest,
  itemListSuccess
} from "../../store/reducers/itemListReducer";

import styled from "styled-components";

const Item = styled.div`
  border-bottom: 1px solid #333;
`;

export class ItemListC extends React.Component {
  componentDidMount() {
    this.props.getItemsRequest();
    getItems()
      .then(data => {
        this.props.getItemsSuccess(data);
      })
      .catch(err => {
        this.props.getItemsFailure();
        console.log(err);
      });
  }

  render() {
    const { requestStatus, items } = this.props;
    console.log({ requestStatus, items });
    if (
      requestStatus === REQUEST_STATUS.NOT_ASKED ||
      requestStatus === REQUEST_STATUS.PENDING
    ) {
      return <h1>Loading</h1>;
    }
    if (requestStatus === REQUEST_STATUS.FAILURE) {
      return <h1>ERROR</h1>;
    }
    // return items.map(i => <p key={i._id}>{JSON.stringify(i)}</p>);

    return items.map(item => (
      <Item
        styles="border: 1px solid #333"
        key={item._id}
        onClick={this.openItem}
      >
        <h3>{item.name}</h3>
        <p>Company: {item.company}</p>
        <p>E-Mail: {item.email}</p>
        <p>Phone: {item.phone}</p>
        {JSON.stringify(item)}
        {/* 5cb6d0e579dc14cfc2fa5053 */}
      </Item>
    ));
  }
}

export const ItemList = connect(
  store => ({
    items: store.itemList.items,
    requestStatus: store.itemList.requestStatus
  }),
  dispatch => ({
    getItemsRequest: () => dispatch(itemListRequest()),
    getItemsSuccess: payload => dispatch(itemListSuccess(payload)),
    getItemsFailure: () => dispatch(itemListFailure())
  })
)(ItemListC);
