import React from "react";
import { connect } from "react-redux";
import { getItem } from "../../api";
import {
  itemFailure,
  itemRequest,
  itemSuccess
} from "../../store/reducers/itemReducer";

export class ItemC extends React.Component {
  componentDidMount() {
    this.props.getItemRequest();
    getItem(this.props.id)
      .then(data => {
        this.props.getItemSuccess(data);
      })
      .catch(err => {
        this.props.getItemFailure();
        console.log(err);
      });
  }
  render() {
    return null;
  }
}

export const Item = connect(
  store => ({
    id: store.item.id,
    itemData: store.item.itemData,
    requestStatus: store.item.requestStatus
  }),
  dispatch => ({
    getItemRequest: () => dispatch(itemRequest()),
    getItemSuccess: payload => dispatch(itemSuccess(payload)),
    getItemFailure: () => dispatch(itemFailure())
  })
)(ItemC);
