import React from "react";
import { Route, Switch } from "react-router-dom";
import { Home } from "./pages/Home/Home";
import { ItemList } from "./pages/ItemList/ItemList";
import { Item } from "./pages/Item/Item";

export const Routes = () => (
  <Switch>
    <Route exact path="/" component={Home} />
    <Route path="/search" component={ItemList} />
    <Route path="/item/:id" component={Item} />
  </Switch>
);
