import { REQUEST_STATUS } from "../../constants";

const ITEM_REQUEST = "ITEM/REQUEST";
const ITEM_SUCCESS = "ITEM/SUCCESS";
const ITEM_FAILURE = "ITEM/FAILURE";

export const itemRequest = () => ({ type: ITEM_REQUEST });
export const itemSuccess = payload => ({ type: ITEM_SUCCESS, payload });
export const itemFailure = () => ({ type: ITEM_FAILURE });

export const item = (
  state = {
    id: "",
    itemData: {},
    requestStatus: REQUEST_STATUS.NOT_ASKED
  },
  { type, payload }
) => {
  switch (type) {
    case ITEM_REQUEST: {
      return { ...state, requestStatus: REQUEST_STATUS.PENDING };
    }
    case ITEM_SUCCESS: {
      return {
        ...state,
        requestStatus: REQUEST_STATUS.SUCCESS,
        id: payload.id,
        itemData: payload
      };
    }
    case ITEM_FAILURE: {
      return { ...state, requestStatus: REQUEST_STATUS.FAILURE, itemData: {} };
    }
  }
  return state;
};
