import { createStore, combineReducers } from "redux";
import { itemList } from "./reducers/itemListReducer";
import { item } from "./reducers/itemReducer";

const rootReducer = combineReducers({
  itemList,
  item
});

export const store = createStore(rootReducer);
